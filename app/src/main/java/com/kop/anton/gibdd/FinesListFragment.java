package com.kop.anton.gibdd;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kop.anton.gibdd.adapters.FinesListAdapter;
import com.kop.anton.gibdd.model.Fine;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * FinesListFragment
 * Created by Anton on 09.09.2016.
 */
public class FinesListFragment extends Fragment {

    private static final String FINES_KEY = "fines";

    @BindView(R.id.fines_list)
    RecyclerView finesList;
    private Unbinder unbinder;
    private ArrayList<Fine> fines = new ArrayList<>();
    private FinesListAdapter adapter;

    public static FinesListFragment newInstance(ArrayList<Fine> fines) {

        Bundle args = new Bundle();
        args.putSerializable(FINES_KEY, fines);
        FinesListFragment fragment = new FinesListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().containsKey(FINES_KEY)) {
                fines = (ArrayList<Fine>) getArguments().getSerializable(FINES_KEY);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fines_list_layout, container, false);
        unbinder = ButterKnife.bind(this, view);
        initList();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void initList() {
        adapter = new FinesListAdapter(fines);
        finesList.setAdapter(adapter);
        finesList.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter.setOnItemClickListener(new FinesListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                FragmentTransaction trans = getFragmentManager().beginTransaction();
                trans.add(R.id.container, FineDetailsFragment.newInstance(adapter.getFine(position)));
                trans.addToBackStack("");
                trans.commit();
            }
        });
    }
}
