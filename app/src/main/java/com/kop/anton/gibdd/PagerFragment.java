package com.kop.anton.gibdd;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * PagerFragment
 * Created by Anton on 27.09.2016.
 */

public class PagerFragment extends Fragment {

    @BindView(R.id.sliding_tabs)
    TabLayout tabLayout;
    @BindView(R.id.pager)
    ViewPager pager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pager_layout, container, false);
        ButterKnife.bind(this, view);
        pager.setPageMargin(12);
        pager.setAdapter(new MyPagerAdapter(getActivity().getSupportFragmentManager()));
        tabLayout.setupWithViewPager(pager);
        return view;
    }


    public class MyPagerAdapter extends FragmentStatePagerAdapter {

        private Fragment[] fragments = new Fragment[]{new SearchByDriverFragment(), new SearchByCarFragment()};

        MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            fragments[position] = fragment;
            return fragment;
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            return fragments[position];
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public String getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getContext().getString(R.string.by_driver).toUpperCase();
                default:
                    return getContext().getString(R.string.by_car).toUpperCase();
            }
        }
    }
}
