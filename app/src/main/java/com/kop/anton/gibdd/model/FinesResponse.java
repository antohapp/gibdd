package com.kop.anton.gibdd.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Anton on 06.09.2016.
 */
public class FinesResponse implements Serializable{

    private int error;
    private ArrayList<Fine> finesList;
    private int count;
    private int inGarage;

    public int getError() {
        return error;
    }

    public ArrayList<Fine> getFinesList() {
        return finesList;
    }

    public int getCount() {
        return count;
    }

    public int getInGarage() {
        return inGarage;
    }
}
