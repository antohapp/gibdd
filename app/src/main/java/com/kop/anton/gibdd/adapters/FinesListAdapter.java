package com.kop.anton.gibdd.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kop.anton.gibdd.R;
import com.kop.anton.gibdd.model.Fine;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * adapter for list of fines
 * Created by Anton on 08.09.2016.
 */
public class FinesListAdapter extends RecyclerView.Adapter<FinesListAdapter.ViewHolder> {

    private ArrayList<Fine> fines;
    private OnItemClickListener listener;

    public FinesListAdapter(ArrayList<Fine> fines) {
        this.fines = fines;
    }

    public Fine getFine(int position) {
        return fines.get(position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fines_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Fine fine = fines.get(position);
        holder.articleCode.setText(fine.getKoapText());
        holder.postedAt.setText(fine.getFineDate());
        holder.postNumber.setText(fine.getBillId());
        holder.totalAmount.setText(String.valueOf(fine.getSum()));
    }

    @Override
    public int getItemCount() {
        if (fines != null) {
            return fines.size();
        } else return 0;
    }

    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.article_code)
        TextView articleCode;
        @BindView(R.id.post_number)
        TextView postNumber;
        @BindView(R.id.posted_at)
        TextView postedAt;
        @BindView(R.id.totalAmount)
        TextView totalAmount;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onItemClick(view, getLayoutPosition());
                    }
                }
            });
        }
    }
}
