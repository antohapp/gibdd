package com.kop.anton.gibdd;

import android.content.Context;

import java.util.regex.Pattern;

/**
 * Helper to check data in search fields
 * Created by Anton on 01.10.2016.
 */

public class SearchHelper {

    private Pattern certPattern;
    private Pattern regPattern;
    private Pattern regionPattern;
    private Pattern driverLicensePattern;

    public SearchHelper(Context context) {
        certPattern = Pattern.compile(context.getString(R.string.cert_pattern));
        regPattern = Pattern.compile(context.getString(R.string.reg_pattern));
        regionPattern = Pattern.compile(context.getString(R.string.region_pattern));
        driverLicensePattern = Pattern.compile(context.getString(R.string.driver_license_pattern));
    }

    public boolean checkCert(CharSequence cert) {
        return certPattern.matcher(cert).matches();
    }

    public boolean checkReg(CharSequence reg) {
        return regPattern.matcher(reg).matches();
    }

    public boolean checkRegion(CharSequence region) {
        return regionPattern.matcher(region).matches();
    }

    public boolean checkdriverLicense(CharSequence driverLicense) {
        return driverLicensePattern.matcher(driverLicense).matches();
    }
}
