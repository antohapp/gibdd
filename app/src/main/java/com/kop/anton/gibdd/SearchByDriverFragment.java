package com.kop.anton.gibdd;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kop.anton.gibdd.model.FinesResponse;
import com.kop.anton.gibdd.network.FinesByDriverRequest;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * SearchByDriverFragment
 * Created by Anton on 27.09.2016.
 */

public class SearchByDriverFragment extends Fragment {

    private final String KEY = "SearchByDriverFragmentKey";

    @BindView(R.id.driverLicense)
    EditText driverLicense;
    @BindView(R.id.searchBTN)
    Button searchBtn;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    private Unbinder unbinder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_by_driver_fragment_layout, container, false);
        unbinder = ButterKnife.bind(this, view);
        driverLicense.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH) {
                    searchFines();
                    return true;
                }
                return false;
            }
        });
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(KEY)) {
                driverLicense.setText(savedInstanceState.getCharSequence(KEY));
            }
        }
        progressBar.getIndeterminateDrawable().setColorFilter(getResources()
                .getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putCharSequence(KEY, driverLicense.getText());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.searchBTN)
    void searchFines() {
        InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (getActivity().getCurrentFocus().getWindowToken() != null) {
            inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
        if (driverLicense.getText().length() > 0) {
            if (new SearchHelper(getContext()).checkdriverLicense(driverLicense.getText())) {
                getFinesByDriver(driverLicense.getText().toString());
            } else {
                driverLicense.setError(getString(R.string.search_format_error));
            }
        } else {
            driverLicense.setError(getString(R.string.search_edit_text_error));
        }
    }

    private void getFinesByDriver(String searchRequestId) {
        progressBar.setVisibility(View.VISIBLE);
        searchBtn.setEnabled(false);
        ((BaseSampleSpiceActivity) getActivity())
                .getSpiceManager()
                .execute(new FinesByDriverRequest(searchRequestId), "", DurationInMillis.ONE_SECOND, new FinesListener());
    }

    private void onSearchStop() {
        progressBar.setVisibility(View.GONE);
        searchBtn.setEnabled(true);
    }

    private class FinesListener implements RequestListener<FinesResponse> {
        @Override
        public void onRequestFailure(SpiceException spiceException) {
            onSearchStop();
            Toast.makeText(getActivity(), spiceException.getLocalizedMessage(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onRequestSuccess(FinesResponse finesResponse) {
            onSearchStop();
            FragmentTransaction trans = getFragmentManager().beginTransaction();
            if (finesResponse.getFinesList() != null && finesResponse.getFinesList().size() > 0) {
                trans.replace(R.id.root, FinesListFragment.newInstance(finesResponse.getFinesList()));
            } else {
                trans.replace(R.id.root, new EmptyFragment());
            }
            trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            trans.addToBackStack(null);
            trans.commit();
        }
    }
}
