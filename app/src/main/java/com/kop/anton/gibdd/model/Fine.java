package com.kop.anton.gibdd.model;

import com.kop.anton.gibdd.converters.DateConverter;

import java.io.Serializable;

/**
 * Fine model
 * Created by Anton on 06.09.2016.
 */
public class Fine implements Serializable {

    // статья нарушения
    private String koapSt;
    private String koapText;
    // Дата постановления (дата постановления не всегда совпадает с датой нарушения
    private String fineDate;
    // сумма штрафа
    private int sum;
    // номер постановления
    private String billId;
    private int hasDiscount;
    private int hasPhoto;
    // я не знаю что это за поле )))
    private int divId;
    // сумма скидки за штраф
    private int discountSum;
    // дата до которой будет действовать скидка
    private String discountUntil;

    public String getKoapSt() {
        return koapSt;
    }

    public String getKoapText() {
        return koapText;
    }

    public String getFineDate() {
        return fineDate;
    }

    public int getSum() {
        return sum;
    }

    public String getBillId() {
        return billId;
    }

    public boolean isHasDiscount() {
        return hasDiscount != 0;
    }

    public boolean isHasPhoto() {
        return hasPhoto != 0;
    }

    public int getDivId() {
        return divId;
    }

    public int getDiscountSum() {
        return discountSum;
    }

    public String getDiscountUntil() {
        return DateConverter.convertDiscountLeft(discountUntil);
    }
}
