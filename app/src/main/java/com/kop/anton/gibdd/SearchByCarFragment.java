package com.kop.anton.gibdd;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kop.anton.gibdd.model.FinesResponse;
import com.kop.anton.gibdd.network.FinesByCarRequest;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * SearchByCarFragment
 * Created by Anton on 27.09.2016.
 */

public class SearchByCarFragment extends Fragment implements TextView.OnEditorActionListener {

    private final String CERT_KEY = "CertKey";
    private final String REG_KEY = "RegKey";
    private final String REGION_KEY = "RegionKey";

    @BindView(R.id.cert)
    EditText cert;
    @BindView(R.id.reg)
    EditText reg;
    @BindView(R.id.region)
    EditText region;
    @BindView(R.id.searchBTN)
    Button searchBtn;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_by_car_fragment_layout, container, false);
        unbinder = ButterKnife.bind(this, view);
        cert.setOnEditorActionListener(this);
        if (savedInstanceState != null) {
            cert.setText(savedInstanceState.getCharSequence(CERT_KEY));
            reg.setText(savedInstanceState.getCharSequence(REG_KEY));
            region.setText(savedInstanceState.getCharSequence(REGION_KEY));
        }
        progressBar.getIndeterminateDrawable().setColorFilter(getResources()
                .getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        return view;
    }

    @OnClick(R.id.searchBTN)
    void searchFines() {
        cert.setError(null);
        reg.setError(null);
        region.setError(null);
        SearchHelper helper = new SearchHelper(getContext());
        if (helper.checkCert(cert.getText()) && helper.checkReg(reg.getText()) && helper.checkRegion(region.getText())) {
            getFinesByCar(reg.getText().toString(), region.getText().toString(), cert.getText().toString());
        } else {
            if (cert.getText().length() <= 0) {
                cert.setError(getString(R.string.search_edit_text_error));
            } else {
                if (!helper.checkCert(cert.getText())) {
                    cert.setError(getString(R.string.search_format_error));
                }
            }
            if (reg.getText().length() <= 0) {
                reg.setError(getString(R.string.search_edit_text_error));
            } else {
                if (!helper.checkReg(reg.getText())) {
                    reg.setError(getString(R.string.search_format_error));
                }
            }
            if (region.getText().length() <= 0) {
                region.setError(getString(R.string.search_edit_text_error));
            } else {
                if (!helper.checkRegion(region.getText())) {
                    region.setError(getString(R.string.search_format_error));
                }
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putCharSequence(CERT_KEY, cert.getText());
        outState.putCharSequence(REG_KEY, reg.getText());
        outState.putCharSequence(REGION_KEY, region.getText());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (i == EditorInfo.IME_ACTION_SEARCH) {
            InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
            searchFines();
            return true;
        }
        return false;
    }

    private void getFinesByCar(String carNumber, String region, String sts) {
        progressBar.setVisibility(View.VISIBLE);
        searchBtn.setEnabled(false);
        ((BaseSampleSpiceActivity) getActivity())
                .getSpiceManager()
                .execute(new FinesByCarRequest(carNumber, region, sts), "", DurationInMillis.ONE_SECOND, new FinesListener());
    }

    private void onSearchStop() {
        progressBar.setVisibility(View.GONE);
        searchBtn.setEnabled(true);
    }

    private class FinesListener implements RequestListener<FinesResponse> {
        @Override
        public void onRequestFailure(SpiceException spiceException) {
            onSearchStop();
            Toast.makeText(getActivity(), spiceException.getLocalizedMessage(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onRequestSuccess(FinesResponse finesResponse) {
            onSearchStop();
            FragmentTransaction trans = getFragmentManager().beginTransaction();
            if (finesResponse.getFinesList() != null && finesResponse.getFinesList().size() > 0) {
                trans.replace(R.id.root_by_car, FinesListFragment.newInstance(finesResponse.getFinesList()));
            } else {
                trans.replace(R.id.root_by_car, new EmptyFragment());
            }
            trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            trans.addToBackStack(null);
            trans.commit();
        }
    }
}
