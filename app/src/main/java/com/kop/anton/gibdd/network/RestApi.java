package com.kop.anton.gibdd.network;

import com.kop.anton.gibdd.model.FinesResponse;
import retrofit.http.GET;
import retrofit.http.Query;


/**
 * Created by Anton on 05.09.2016.
 */
public interface RestApi {

    @GET("/fines_gibdd_driver")
    FinesResponse getFinesByDriver(@Query("vu") String driverLicense);

    @GET("/fines_gibdd")
    FinesResponse getFinesByCar(@Query("num") String carNumber, @Query("reg") String region, @Query("sts") String sts);
}
