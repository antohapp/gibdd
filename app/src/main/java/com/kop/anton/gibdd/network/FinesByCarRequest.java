package com.kop.anton.gibdd.network;

import com.kop.anton.gibdd.model.FinesResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by Anton on 26.09.2016.
 */

public class FinesByCarRequest extends RetrofitSpiceRequest<FinesResponse, RestApi> {

    private String carNumber;
    private String region;
    private String sts;

    public FinesByCarRequest(String carNumber, String region, String sts) {
        super(FinesResponse.class, RestApi.class);
        this.carNumber = carNumber;
        this.region = region;
        this.sts = sts;
    }

    @Override
    public FinesResponse loadDataFromNetwork() throws Exception {
        return getService().getFinesByCar(carNumber, region, sts);
    }
}
