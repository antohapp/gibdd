package com.kop.anton.gibdd;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;


import com.kop.anton.gibdd.databinding.FineDetailsFragmentLayoutBinding;
import com.kop.anton.gibdd.model.Fine;

/**
 * FineDetailsFragment
 * Created by Anton on 22.09.2016.
 */

public class FineDetailsFragment extends Fragment {

    private final static String FINE_KEY = "fine key";
    private Fine fine;


    public static FineDetailsFragment newInstance(Fine fine) {
        Bundle args = new Bundle();
        args.putSerializable(FINE_KEY, fine);
        FineDetailsFragment fragment = new FineDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().containsKey(FINE_KEY)) {
                fine = (Fine) getArguments().getSerializable(FINE_KEY);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FineDetailsFragmentLayoutBinding layoutBinding = DataBindingUtil.inflate(inflater, R.layout.fine_details_fragment_layout, container, false);
        View view = layoutBinding.getRoot();
        if (fine != null) {
            layoutBinding.setFine(fine);
        }
        LinearLayout layout = (LinearLayout) view.findViewById(R.id.fine_details_root);
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return view;
    }
}
