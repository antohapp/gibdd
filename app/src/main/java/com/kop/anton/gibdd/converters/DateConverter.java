package com.kop.anton.gibdd.converters;

import android.support.annotation.NonNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * converter to convert dates
 * Created by Anton on 04.11.2016.
 */

public class DateConverter {

    private static final String DATE_FORMAT = "yyyy-MM-dd";

    @NonNull
    public static String convertDiscountLeft(String s) {
        Date date = new Date();
        try {
            date =  new SimpleDateFormat(DATE_FORMAT, Locale.US).parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long dif = date.getTime() - new Date().getTime();
        if (dif < 0) {
            dif = 0;
        }
        return String.valueOf(TimeUnit.MILLISECONDS.toDays(dif));
    }
}
