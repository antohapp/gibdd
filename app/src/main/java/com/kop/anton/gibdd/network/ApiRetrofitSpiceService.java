package com.kop.anton.gibdd.network;

import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;


/**
 * Created by Anton on 05.09.2016.
 */
public class ApiRetrofitSpiceService extends RetrofitGsonSpiceService {

    private final static String BASE_URL = "https://shtrafyonline.ru/ajax";

    @Override
    public void onCreate() {
        super.onCreate();
        addRetrofitInterface(RestApi.class);
    }


    @Override
    protected String getServerUrl() {
        return BASE_URL;
    }
}
