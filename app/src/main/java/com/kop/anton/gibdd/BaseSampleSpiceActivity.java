package com.kop.anton.gibdd;

import android.support.v7.app.AppCompatActivity;
import com.kop.anton.gibdd.network.ApiRetrofitSpiceService;
import com.octo.android.robospice.SpiceManager;

/**
 * Created by Anton on 05.09.2016.
 */
public class BaseSampleSpiceActivity extends AppCompatActivity {
    private SpiceManager spiceManager = new SpiceManager(ApiRetrofitSpiceService.class);

    @Override
    protected void onStart() {
        spiceManager.start(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        spiceManager.shouldStop();
        super.onStop();
    }

    public SpiceManager getSpiceManager() {
        return spiceManager;
    }
}
