package com.kop.anton.gibdd.network;

import com.kop.anton.gibdd.model.FinesResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by Anton on 06.09.2016.
 */
public class FinesByDriverRequest extends RetrofitSpiceRequest<FinesResponse, RestApi> {

    private String driverLicense;

    public FinesByDriverRequest(String driverLicense) {
        super(FinesResponse.class, RestApi.class);
        this.driverLicense = driverLicense;
    }

    @Override
    public FinesResponse loadDataFromNetwork() throws Exception {
        return getService().getFinesByDriver(driverLicense);
    }
}
