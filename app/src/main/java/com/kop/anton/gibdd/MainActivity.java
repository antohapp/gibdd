package com.kop.anton.gibdd;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.Fragment;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

public class MainActivity extends BaseSampleSpiceActivity {

    private final String TAG = "tag";
    private final String KEY = "key";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);
        getSupportActionBar().setElevation(0);
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(KEY)) {
                Fragment fragment = getSupportFragmentManager().findFragmentByTag(TAG);
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, fragment, TAG)
                        .commit();
            }

        } else {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, new PagerFragment(), TAG)
                    .commit();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putString(KEY, TAG);
    }
}
